import { _decorator, Component, Label, Layout, Size, UITransform } from "cc";
import { CellData, GameStateData, UserData } from "../ModelDatas/Data";
import { cloneItemFromNode, effectIncrease, getRandomInt } from "../Utils/Util";
import PoolManager from "../Managers/PoolManager";
import { ItemLevelComp, ItemLevelData } from "../Items/ItemLevelComp";
import UIManager from "../Managers/UIManager";
import SettingPopup from "../Popups/SettingPopup";
import { ItemCardComp, ItemCardData } from "../Items/ItemCardComp";
import { UserManager } from "../Managers/UserManager";
import ResourceManager from "../Managers/ResourceManager";
import { AnimComp } from "../Components/AnimComp";
import SoundManager from "../Managers/SoundManager";
import { SoundName } from "../Defines/SoundName";
const { ccclass, property } = _decorator;

const MaxValue: number = 42;
const GameLevels: number[][][] = [
  [
    [0, 0, 0],
    [0, 0, 0],
  ],
  [
    [0, 0, 0],
    [0, 1, 0],
    [0, 0, 0],
  ],
  [
    [0, 0, 0, 0],
    [0, 0, 0, 0],
    [0, 0, 0, 0],
    [0, 0, 0, 0],
  ],
  [
    [0, 0, 0, 0, 0],
    [0, 0, 1, 0, 0],
    [0, 0, 1, 0, 0],
    [0, 0, 0, 0, 0],
  ],
  [
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0],
  ],
];

@ccclass("StartController")
export class StartController extends Component {
  @property(ItemLevelComp)
  itemLevel: ItemLevelComp = null;

  @property(ItemCardComp)
  itemCard: ItemCardComp = null;

  @property(AnimComp)
  startNode: AnimComp = null;

  @property(AnimComp)
  gameNode: AnimComp = null;

  @property(AnimComp)
  endGameNode: AnimComp = null;

  @property(Layout)
  layoutCard: Layout = null;

  @property(Label)
  matches: Label = null;

  @property(Label)
  turns: Label = null;

  @property(Label)
  endLabel: Label = null;

  private currentCardSelect: ItemCardComp = null;
  private levelItems: ItemLevelComp[] = [];
  private cardItems: ItemCardComp[] = [];
  private currentUserData: UserData = null;
  private currentGameState: GameStateData = null;
  private currentLevel: number = 0;
  private endGame: boolean = false;
  private winLose: boolean = false;
  protected onLoad(): void {
    this.startNode.node.active = false;
    this.gameNode.node.active = false;
    this.endGameNode.node.active = false;
    ResourceManager.initBundle(null, this.loadComplete.bind(this));
  }

  loadComplete() {
    SoundManager.instance.setMusicVolume(0.3, true);
    SoundManager.instance.playBgMusicLocalName(SoundName.BGM);
    this.startInit();
  }

  startInit() {
    this.currentUserData = UserManager.userData;
    for (let index = 0; index < GameLevels.length; index++) {
      this.createLevelItem(index);
    }
    this.startNode.node.active = true;
    this.gameNode.node.active = false;
    this.endGameNode.node.active = false;
    this.startNode.showAnim();
    this.onClickLevelItem(this.currentUserData.currentLevel);
  }

  //------------ Event Button -------------------------
  onStartGame() {
    SoundManager.instance.playEffLocalName(SoundName.SFX_CLICK);
    this.endGame = false;
    this.startNode.hideAnim();
    this.gameNode.showAnim();
    this.updateScore();
    this.clearCardItem();
    this.resizeCard();
    this.currentGameState.cells.forEach((cells) => {
      cells.forEach((cellData) => {
        this.createCardItem(cellData);
      });
    });
  }

  onNextGame() {
    SoundManager.instance.playEffLocalName(SoundName.SFX_CLICK);
    if (this.winLose) {
      this.currentLevel++;
      if (this.currentLevel >= GameLevels.length) {
        this.currentLevel = 0;
      }
      this.updateLevel();
    }
    this.currentGameState = this.onNewGameState(this.currentLevel);
    this.endGameNode.hideAnim();
    this.onStartGame();
  }

  onHome() {
    if (this.endGame) return;
    SoundManager.instance.playEffLocalName(SoundName.SFX_CLICK);
    this.startNode.showAnim();
    this.gameNode.hideAnim();
    UserManager.saveGameState(this.currentUserData, this.currentGameState);
  }

  onClickSetting() {
    SoundManager.instance.playEffLocalName(SoundName.SFX_CLICK);
    UIManager.instance.openPopup(SettingPopup,"SettingPopup");
  }

  //------------ End Event Button ----------------------

  //------------ Logic Game Level ----------------------
  private createLevelItem(level: number) {
    if (!this.itemLevel) return;
    let newItem: ItemLevelComp = null;
    let node = PoolManager.instance.getPool(this.itemLevel.name);
    if (node == null) {
      newItem = cloneItemFromNode(ItemLevelComp, this.itemLevel.node);
    } else {
      newItem = node.getComponent(ItemLevelComp);
    }
    newItem.setData(
      new ItemLevelData(level, () => {
        this.onClickLevelItem(level);
      })
    );
    newItem.node.setParent(this.itemLevel.node.parent);
    this.levelItems.push(newItem);
  }

  private getRandomIntColum(c: number, gameState: GameStateData): number {
    let random = getRandomInt(c);
    if (gameState.cells[random].find((x) => x.value == -1) != null) {
      return random;
    } else {
      return this.getRandomIntColum(c, gameState);
    }
  }

  private getRandomIntRow(c: number, r: number, gameState: GameStateData): number {
    let random = getRandomInt(r);
    if (gameState.cells[c].find((x) => x.row == random && x.value == -1) != null) {
      return random;
    } else {
      return this.getRandomIntRow(c, r, gameState);
    }
  }

  private getRandomValue(gameState: GameStateData): number {
    let random = getRandomInt(MaxValue);
    let check = true;
    gameState.cells.forEach((element) => {
      if (element.find((x) => x.value == random) != null) {
        check = false;
      }
    });
    if (check) {
      return random;
    } else {
      return this.getRandomValue(gameState);
    }
  }

  private onClickLevelItem(level: number) {
    this.currentLevel = level;
    this.updateLevel();
    this.currentGameState = this.currentUserData.gameStateData.find((x) => x.level == level);
    if (this.currentGameState == null) {
      this.currentGameState = this.onNewGameState(level);
    }
    if (this.currentGameState.endState) {
      this.currentGameState = this.onNewGameState(level);
    }
  }

  private updateLevel() {
    this.levelItems.forEach((item, index) => {
      item.setActive(index == this.currentLevel);
    });
    this.currentUserData.currentLevel = this.currentLevel;
    UserManager.userData = this.currentUserData;
  }

  private onNewGameState(level: number): GameStateData {
    let gameLevel = GameLevels[level];
    let gameState = new GameStateData(level, gameLevel);
    let maxColum = gameLevel.length;
    let maxRow = gameLevel[0].length;
    for (let index = 0; index < gameState.maxMatches; index++) {
      let value = this.getRandomValue(gameState);
      let c = this.getRandomIntColum(maxColum, gameState);
      let r = this.getRandomIntRow(c, maxRow, gameState);

      gameState.cells[c][r].value = value;

      let c2 = this.getRandomIntColum(maxColum, gameState);
      let r2 = this.getRandomIntRow(c2, maxRow, gameState);
      gameState.cells[c2][r2].value = value;
    }
    UserManager.saveGameState(this.currentUserData, gameState);
    return gameState;
  }
  //------------ End Logic Game Level ----------------------

  //------------ Logic Game ----------------------------

  private updateScore() {
    this.matches.string = this.currentGameState.matches + "";
    this.turns.string = this.currentGameState.turns + "";
  }

  private resizeCard() {
    let maxColum = this.currentGameState.cells.length;
    let maxRow = this.currentGameState.cells[0].length;
    let height = this.layoutCard.getComponent(UITransform).height - this.layoutCard.paddingTop - this.layoutCard.paddingBottom - this.layoutCard.spacingY * (maxColum - 1);

    let cellHeight = height / maxColum;
    this.layoutCard.getComponent(UITransform).width = cellHeight * maxRow + this.layoutCard.paddingLeft + this.layoutCard.paddingRight + this.layoutCard.spacingX * (maxRow - 1);

    this.layoutCard.cellSize = new Size(cellHeight, cellHeight);
  }

  private checkAndShowWinGame() {
    if (this.currentGameState.matches == this.currentGameState.maxMatches) {
      this.currentGameState.endState = true;
      this.endGame = true;
      this.winLose = true;
      this.scheduleOnce(() => {
        this.endGameNode.showAnim();
        this.endLabel.string = "YOU WIN!";
        this.gameNode.hideAnim();
        SoundManager.instance.playEffLocalName(SoundName.SFX_WIN);
        UserManager.saveGameState(this.currentUserData, this.currentGameState);
      }, 0.5);
    }
  }

  private checkAndShowLoseGame() {
    if (this.currentGameState.turns >= this.currentGameState.maxMatches * 2) {
      this.currentGameState.endState = true;
      this.endGame = true;
      this.winLose = false;
      this.endLabel.string = "GAME OVER!";
      this.endGameNode.showAnim();
      this.gameNode.hideAnim();
      SoundManager.instance.playEffLocalName(SoundName.SFX_GAME_OVER);
      UserManager.saveGameState(this.currentUserData, this.currentGameState);
    }
  }

  private showWinCard(selectCard: ItemCardComp) {
    this.currentGameState.matches++;
    this.currentGameState.turns++;
    selectCard.winCard();
    this.currentCardSelect.winCard();
    this.updateScore();
    effectIncrease(this.matches.node);
    effectIncrease(this.turns.node);
    this.checkAndShowWinGame();
  }

  private showLoseCard(selectCard: ItemCardComp) {
    this.currentGameState.turns++;
    selectCard.hideCard();
    if (this.currentCardSelect) this.currentCardSelect.hideCard();
    this.updateScore();
    effectIncrease(this.turns.node);
    this.checkAndShowLoseGame();
  }

  private checkCardWin(itemCard1: ItemCardComp, itemCard2: ItemCardComp): boolean {
    if (itemCard1 == null) return false;
    if (itemCard1.data.cellData.value == itemCard2.data.cellData.value) {
      return true;
    }
    return false;
  }

  private onClickCardItem(selectCard: ItemCardComp) {
    selectCard.showCard();
    if (this.currentCardSelect != null) {
      if (this.checkCardWin(this.currentCardSelect, selectCard)) {
        this.showWinCard(selectCard);
      } else {
        this.showLoseCard(selectCard);
      }
      this.currentCardSelect = null;
    } else {
      this.currentCardSelect = selectCard;
    }
  }

  private createCardItem(cellData: CellData) {
    if (!this.itemCard) return;
    let newItem: ItemCardComp = null;
    let node = PoolManager.instance.getPool(this.itemCard.name);
    if (node == null) {
      newItem = cloneItemFromNode(ItemCardComp, this.itemCard.node);
    } else {
      newItem = node.getComponent(ItemCardComp);
    }
    newItem.setData(
      new ItemCardData(cellData, () => {
        this.onClickCardItem(newItem);
      })
    );
    newItem.node.setParent(this.itemCard.node.parent);
    this.cardItems.push(newItem);
  }

  private clearCardItem() {
    this.currentCardSelect = null;
    this.cardItems.forEach((element) => {
      PoolManager.instance.putPool(element.node.name, element.node);
    });
    this.cardItems = [];
  }
  //------------ End Logic Game ----------------------------
}
