import { _decorator, Component, Node, tween, Tween, UIOpacity, Vec3 } from "cc";
const { ccclass, property } = _decorator;

@ccclass("AnimComp")
export class AnimComp extends Component {
  private mid: Node = null;
  private left: Node = null;
  private top: Node = null;
  protected onLoad(): void {
    this.mid = this.node.getChildByName("Mid");
    this.left = this.node.getChildByName("Left");
    this.top = this.node.getChildByName("Top");
  }

  protected onDestroy(): void {
    Tween.stopAllByTarget(this.node);
    Tween.stopAllByTarget(this.top);
    Tween.stopAllByTarget(this.left);
    Tween.stopAllByTarget(this.mid);
  }

  showAnim() {
    Tween.stopAllByTarget(this.node);
    this.node.active = true;
    if (this.top) {
      let root = this.top.getChildByName("Root");
      if (root) {
        root.setPosition(0, 640, 0);
        Tween.stopAllByTarget(root);
        tween(root).to(0.2, { position: Vec3.ZERO }, { easing: "backOut" }).start();
      } else {
        this.top.active = true;
      }
    }
    if (this.left) {
      let root = this.left.getChildByName("Root");
      if (root) {
        root.setPosition(-640, 0, 0);
        Tween.stopAllByTarget(root);
        tween(root).to(0.2, { position: Vec3.ZERO }, { easing: "backOut" }).start();
      } else {
        this.left.active = true;
      }
    }
    if (this.mid) {
      let root = this.mid.getChildByName("Root");
      if (root) {
        let uio = root.getComponent(UIOpacity);
        if (uio == null) uio = root.addComponent(UIOpacity);
        uio.opacity = 0;
        Tween.stopAllByTarget(uio);
        tween(uio).to(0.2, { opacity: 255 }).start();
      } else {
        this.mid.active = true;
      }
    }
  }

  hideAnim() {
    if (this.top) {
      let root = this.top.getChildByName("Root");
      if (root) {
        Tween.stopAllByTarget(root);
        tween(root)
          .to(0.2, { position: new Vec3(0, 640, 0) }, { easing: "backIn" })
          .start();
      } else {
        this.top.active = false;
      }
    }
    if (this.left) {
      let root = this.left.getChildByName("Root");
      if (root) {
        Tween.stopAllByTarget(root);
        tween(root)
          .to(0.2, { position: new Vec3(-640, 0, 0) }, { easing: "backIn" })
          .start();
      } else {
        this.left.active = false;
      }
    }
    if (this.mid) {
      let root = this.mid.getChildByName("Root");
      if (root) {
        let uio = root.getComponent(UIOpacity);
        if (uio == null) uio = root.addComponent(UIOpacity);
        Tween.stopAllByTarget(uio);
        tween(uio).to(0.2, { opacity: 0 }).start();
      } else {
        this.mid.active = false;
      }
    }
    Tween.stopAllByTarget(this.node);
    tween(this.node)
      .delay(0.2)
      .call(() => {
        this.node.active = false;
      })
      .start();
  }
}
