import { _decorator, Component, view, Size, screen, ResolutionPolicy } from "cc";
const { ccclass } = _decorator;
@ccclass("AutoCanvasSizeComp")
export default class AutoCanvasSizeComp extends Component {
  private readonly defaultRatio: number = 16 / 9;
  private startSize: Size = new Size(1280, 720);
  onLoad() {
    screen.on("window-resize", this.setFlexibleWebScreen.bind(this), this);
  }

  private setFlexibleWebScreen() {
    let size = screen.windowSize;
    let w = size.width;
    let h = size.height;
    let ratio = w / h;
    let fitHeight = ratio > this.defaultRatio;
    if (fitHeight) {
      view.setDesignResolutionSize(this.startSize.width, this.startSize.height, ResolutionPolicy.FIXED_HEIGHT);
    } else {
      view.setDesignResolutionSize(this.startSize.width, this.startSize.height, ResolutionPolicy.FIXED_WIDTH);
    }
  }
}
