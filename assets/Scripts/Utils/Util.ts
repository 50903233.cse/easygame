import { Component, Node, Tween, UIOpacity, Vec3, instantiate, tween } from "cc";

export function effectIncrease(node: Node, scale: number = 2, duration: number = 0.5) {
  let eff = instantiate(node);
  eff.setParent(node.parent);
  eff.setPosition(node.position);
  eff.scale = Vec3.ONE;
  if (eff.getComponent(UIOpacity) == null) eff.addComponent(UIOpacity);
  eff.getComponent(UIOpacity).opacity = 255;
  Tween.stopAllByTarget(eff);
  tween(eff)
    .to(duration, { scale: new Vec3(scale, scale, scale) }, { easing: "circOut" })
    .start();
  tween(eff.getComponent(UIOpacity))
    .to(duration, { opacity: 0 }, { easing: "circOut" })
    .call(() => {
      eff.destroy();
    })
    .start();
}

export function cloneItemFromNode<T extends Component>(item: new () => T, node: Node): T {
  if (!node) return null;
  let newNode = instantiate(node);

  let newItem = newNode.getComponent(item);
  newItem.node.active = true;
  newItem.node.name = node.name;
  node.active = false;
  return newItem;
}

export function getRandomInt(max: number, min: number = 0): number {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min;
}
