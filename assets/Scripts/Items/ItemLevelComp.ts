import { _decorator, Component, Label, Node } from "cc";
import SoundManager from "../Managers/SoundManager";
import { SoundName } from "../Defines/SoundName";
const { ccclass, property } = _decorator;

@ccclass("ItemLevelData")
export class ItemLevelData {
  level: number = 0;
  onClick: Function = null;
  constructor(level: number, onClick: Function = null) {
    this.level = level;
    this.onClick = onClick;
  }
}

@ccclass("ItemLevelComp")
export class ItemLevelComp extends Component {
  @property(Label)
  nameLabel: Label = null;

  @property(Node)
  bg: Node = null;

  @property(Node)
  active: Node = null;

  private data: ItemLevelData;
  private getName() {
    if (this.data.level == 0) return "Easy";
    if (this.data.level == 2) return "Medium";
    if (this.data.level == 4) return "Hard";
    return "";
  }

  setData(data: ItemLevelData) {
    this.data = data;
    this.nameLabel.string = this.getName();
  }

  setActive(active: boolean) {
    //this.bg.active = !active;
    this.active.active = active;
  }

  onClick() {
    if (this.data && this.data.onClick) {
      SoundManager.instance.playEffLocalName(SoundName.SFX_CLICK);
      this.data.onClick();
    }
  }
}
