import { _decorator, Component, Node, Sprite, tween, Tween, Vec3 } from "cc";
import { CellData } from "../ModelDatas/Data";
import ResourceManager from "../Managers/ResourceManager";
import SoundManager from "../Managers/SoundManager";
import { SoundName } from "../Defines/SoundName";
const { ccclass, property } = _decorator;

@ccclass("ItemCardData")
export class ItemCardData {
  cellData: CellData = null;
  onClick: Function = null;
  constructor(cellData: CellData, onClick: Function = null) {
    this.cellData = cellData;
    this.onClick = onClick;
  }
}

@ccclass("ItemCardComp")
export class ItemCardComp extends Component {
  @property(Node)
  root: Node = null;

  @property(Node)
  back: Node = null;

  @property(Node)
  front: Node = null;

  @property(Sprite)
  icon: Sprite = null;

  data: ItemCardData;
  private started: boolean = false;
  protected onDestroy(): void {
    this.unscheduleAllCallbacks();
  }
  setData(data: ItemCardData) {
    this.data = data;
    this.started = false;
    this.root.active = this.data.cellData.value > -2;
    if (this.root.active) {
      ResourceManager.loadSprite(this.icon, data.cellData.value);
      this.root.setScale(1, 1, 1);
      this.root.active = !this.data.cellData.match;
      this.onActive(true);
      this.scheduleOnce(this.startCard.bind(this), 1);
    }
  }

  onActive(active: boolean) {
    this.back.active = !active;
    this.front.active = active;
  }

  onClick() {
    if (!this.started) return;
    if (!this.root.active) return;
    if (this.data && this.data.onClick && !this.data.cellData.active) {
      this.data.onClick();
    }
  }

  startCard() {
    this.onActive(this.data.cellData.active);
    if (this.data.cellData.active && !this.data.cellData.match && this.data.onClick) {
      this.data.onClick();
    }
    this.started = true;
  }

  showCard() {
    if (this.data.cellData.match) return;
    this.unscheduleAllCallbacks();
    SoundManager.instance.playOneEffLocalName(SoundName.SFX_FLIP_CARD);
    this.data.cellData.active = true;
    Tween.stopAllByTarget(this.root);
    tween(this.root)
      .to(0.1, { scale: new Vec3(0, 1, 1) })
      .call(() => {
        this.onActive(true);
      })
      .to(0.1, { scale: new Vec3(1, 1, 1) })
      .start();
  }

  hideCard() {
    if (this.data.cellData.match) return;
    this.data.cellData.active = false;
    SoundManager.instance.playOneEffLocalName(SoundName.SFX_ERROR);
    tween(this.root)
      .delay(0.4)
      .to(0.1, { scale: new Vec3(0, 1, 1) })
      .call(() => {
        this.onActive(false);
      })
      .to(0.1, { scale: new Vec3(1, 1, 1) })
      .start();
  }

  winCard() {
    this.data.cellData.match = true;
    SoundManager.instance.playOneEffLocalName(SoundName.SFX_SUCCESS);
    tween(this.root)
      .delay(0.4)
      .to(0.1, { scale: new Vec3(0, 0, 0) }, { easing: "circIn" })
      .call(() => {
        this.root.active = false;
      })
      .start();
  }
}
