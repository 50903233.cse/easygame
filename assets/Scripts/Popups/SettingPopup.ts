import { _decorator, Component, Toggle } from "cc";
const { ccclass, property } = _decorator;

import PopUpInstance from "../Bases/PopUpInstance";
import SoundManager from "../Managers/SoundManager";
import { SoundName } from "../Defines/SoundName";

@ccclass("SettingPopup")
export default class SettingPopup extends PopUpInstance {
  @property(Toggle)
  sfx: Toggle | null = null;
  @property(Toggle)
  music: Toggle | null = null;
  onLoad(): void {
    super.onLoad();
    {
      var checkEvent = new Component.EventHandler();
      checkEvent.target = this.node;
      checkEvent.component = "SettingPopup";
      checkEvent.handler = "onChangeSfx";
      checkEvent.customEventData = "";
      this.sfx.checkEvents.push(checkEvent);
    }

    {
      var checkEvent = new Component.EventHandler();
      checkEvent.target = this.node;
      checkEvent.component = "SettingPopup";
      checkEvent.handler = "onChangeMusic";
      checkEvent.customEventData = "";
      this.music.checkEvents.push(checkEvent);
    }
  }
  protected onShow(data: any): void {
    this.sfx.isChecked = SoundManager.instance.getSfxStatusFromStorage();
    this.music.isChecked = SoundManager.instance.getMusicStatusFromStorage();
  }
  onChangeSfx() {
    SoundManager.instance.playOneEffLocalName(SoundName.SFX_CLICK);
    this.sfx.target.active = !this.sfx.isChecked;
    SoundManager.instance.setSfxStatus(this.sfx.isChecked);
    SoundManager.instance.playOneEffLocalName(SoundName.SFX_CLICK);
  }
  onChangeMusic() {
    SoundManager.instance.playOneEffLocalName(SoundName.SFX_CLICK);
    this.music.target.active = !this.music.isChecked;
    SoundManager.instance.setMusicStatus(this.music.isChecked);
  }
}
