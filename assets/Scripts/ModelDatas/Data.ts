import { log } from "cc";

export class UserData {
  currentLevel: number = 0;
  gameStateData: GameStateData[] = [];
  constructor(currentLevel: number) {
    this.currentLevel = currentLevel;
    this.gameStateData = [];
  }
}

export class CellData {
  col: number;
  row: number;
  value: number;
  active: boolean;
  match: boolean;
  constructor(col: number, row: number, value: number) {
    this.col = col;
    this.row = row;
    this.value = value;
    this.active = false;
    this.match = false;
  }
}

export class GameStateData {
  level: number = 0;
  cells: CellData[][] = [];
  matches: number = 0;
  maxMatches: number = 0;
  turns: number = 0;
  endState: boolean = false;
  constructor(level: number, gameLevel: number[][]) {
    this.level = level;
    this.cells = [];
    for (let c = 0; c < gameLevel.length; c++) {
      let cells: CellData[] = [];
      for (let r = 0; r < gameLevel[c].length; r++) {
        let data = gameLevel[c][r];
        let value = data == 0 ? -1 : -2;
        let cellData = new CellData(c, r, value);
        cells.push(cellData);
      }
      this.cells.push(cells);
    }
    this.endState = false;
    this.matches = 0;
    this.turns = 0;
    let check = 0;
    let c = 0;
    let r = 0;
    gameLevel.forEach((element, cw) => {
      element.forEach((e, rw) => {
        if (e == 0) {
          check++;
          (c = cw), (r = rw);
        }
      });
    });
    if (check % 2 == 1) {
      log("data ERROR.................");
      gameLevel[c][r] = 1;
      check--;
    }
    this.maxMatches = check / 2;
  }
}
