import { _decorator } from "cc";
export class SoundName {
  public static BGM: string = "Bgm";

  public static SFX_CLICK: string = "click";
  public static SFX_FLIP_CARD: string = "flipcard";
  public static SFX_GAME_OVER: string = "gameover";
  public static SFX_SUCCESS: string = "success";
  public static SFX_ERROR: string = "error";
  public static SFX_WIN: string = "youwin";

}
