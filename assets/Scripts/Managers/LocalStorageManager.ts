import { _decorator, sys } from "cc";
import { UserData } from "../ModelDatas/Data";
const { ccclass, property } = _decorator;

@ccclass("LocalStorageManager")
export default class LocalStorageManager {
  public static internalGetString(key: string, defaultValue: string = ""): string {
    const isValue = sys.localStorage.getItem(key);

    return isValue == undefined ? defaultValue : isValue;
  }
  public static internalSaveString(key: string, strValue: string = ""): void {
    if (strValue == undefined) {
      sys.localStorage.removeItem(key);
    } else {
      sys.localStorage.setItem(key, strValue);
    }
  }
  public static internalSaveBoolean(key: string, defaultValue: boolean = true): void {
    if (defaultValue == undefined) {
      sys.localStorage.removeItem(key);
    } else {
      sys.localStorage.setItem(key, defaultValue);
    }
  }
  public static internalGetBoolean(key: string, defaultValue: boolean = true): boolean {
    let isValue = sys.localStorage.getItem(key);
    if (isValue == undefined) {
      isValue = defaultValue;
    }
    if (typeof isValue === "string") {
      return isValue === "true";
    } else {
      return isValue;
    }
  }
  public static internalSaveNum(key: string, numValue: number = 0): void {
    if (numValue == undefined) {
      sys.localStorage.removeItem(key);
    } else {
      sys.localStorage.setItem(key, numValue);
    }
  }
  public static internalGetNum(key: string, defaultValue: number = 0): number {
    let isValue = sys.localStorage.getItem(key);
    if (isValue == undefined) {
      isValue = defaultValue;
    }
    if (typeof isValue === "string") {
      return +isValue;
    } else {
      return isValue;
    }
  }
  public static internalSaveUserData(key: string, userData: UserData): void {
    if (userData == undefined) {
      sys.localStorage.removeItem(key);
    } else {
      let userSaveData = JSON.stringify(userData);
      sys.localStorage.setItem(key, userSaveData);
    }
  }
  public static internalGetUserData(key: string, userData: UserData):UserData {
    let isValue = JSON.parse(sys.localStorage.getItem(key)) as UserData;
    if (isValue == undefined) {
      isValue = userData;
    }
    return isValue;
  }
}
