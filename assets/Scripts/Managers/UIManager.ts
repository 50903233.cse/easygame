import { _decorator, Component, director, instantiate, Node, Vec3 } from "cc";
const { ccclass, property } = _decorator;

import PopUpInstance from "../Bases/PopUpInstance";
import ResourceManager from "./ResourceManager";

@ccclass("UIManager")
export default class UIManager extends Component {
  private static _instance: UIManager = null;
  public static get instance(): UIManager {
    if (UIManager._instance == null) {
      let canvas = director.getScene().getChildByName("Canvas");
      let i = canvas.getComponentInChildren(UIManager);
      UIManager._instance = i;
    }
    return UIManager._instance;
  }

  private _popupInstances: Map<string, PopUpInstance> = new Map<string, PopUpInstance>();
  private _currentPopup: PopUpInstance;
  private _currentPopupName: string;
  protected onDestroy(): void {
    UIManager._instance = null;
  }
  public closePopup(type: typeof PopUpInstance, name: string) {
    let self = UIManager._instance;
    self.getPopupInstance(type, name, (instance) => {
      instance.closeInstance();
    });
  }
  public openPopup(type: typeof PopUpInstance, name: string, data = null, onComplete: (instance: PopUpInstance) => void = null, parent: Node | null = null, onYes: () => void = null, onNo: () => void = null) {
    let self = UIManager._instance;
    parent = parent ? parent : self.node;
    if (name == self._currentPopupName && self._currentPopup?.node) {
      if (onComplete) onComplete(self._currentPopup);
      return;
    }
    self._currentPopupName = name;
    self.getPopupInstance(
      type,
      name,
      (instance) => {
        if (self._currentPopup == instance) {
          if (onComplete) onComplete(self._currentPopup);
          return;
        }
        self._currentPopup = instance;
        instance.open(data, onYes, onNo);
        instance._close = () => {
          self._currentPopup = null;
          self._currentPopupName = "";
        };
        if (onComplete) onComplete(self._currentPopup);
      },
      parent
    );
  }

  public getPopupInstance(type: typeof PopUpInstance, name: string, onComplete: (instance: PopUpInstance) => void, parent: Node = null) {
    let self = UIManager._instance;
    const _parent = parent == null ? this.node : parent;
    if (self._popupInstances.has(name)) {
      let popup = self._popupInstances.get(name);
      popup.node.parent = null;
      _parent.addChild(popup.node);
      popup.node.setPosition(Vec3.ZERO);
      onComplete(popup);
    } else {
      this.loadPopUpPrefab(type, name, onComplete, _parent);
    }
  }
  private loadPopUpPrefab(type: typeof PopUpInstance, name: string, onComplete: (instance: PopUpInstance) => void, parent: Node | null = null) {
    let self = UIManager._instance;
    let prefab = ResourceManager.getPopupPrefab(name);
    if (prefab == null) {
      return;
    }
    const newNode = instantiate(prefab) as unknown as Node;
    const instance = newNode.getComponent(type);
    parent.addChild(newNode);
    newNode.setPosition(Vec3.ZERO);
    self._popupInstances.set(name, instance);
    onComplete(instance);
  }
}
