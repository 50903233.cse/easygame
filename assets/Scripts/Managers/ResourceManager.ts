import { _decorator, AudioClip, SpriteFrame, Prefab, AssetManager, Sprite, Asset, log, director, sp, resources } from "cc";
const { ccclass, property } = _decorator;
@ccclass("ResourceManager")
export default class ResourceManager {
  private static sounds: Map<string, AudioClip> = new Map<string, AudioClip>();
  private static sprites: Map<string, SpriteFrame> = new Map<string, SpriteFrame>();
  private static popups: Map<string, Prefab> = new Map<string, Prefab>();
  public static clear() {
    this.sounds.clear();
    this.sprites.clear();
    this.popups.clear();
    this.bundle.releaseAll();
  }
  public static initBundle(onProgress?: (finish: number, total: number) => void, onComplete?: Function) {
    let c1 = 0;
    let t1 = 100;
    let c2 = 0;
    let t2 = 100;
    let c3 = 0;
    let t3 = 100;
    let progress = () => {
      if (onProgress) onProgress(c1 + c2 + c3, t1 + t2 + t3);
    };
    this.initPopups(
      () => {
        this.initSounds(
          () => {
            this.initSprites(onComplete, (c, t, i) => {
              c3 = c;
              t3 = t;
              progress();
            });
          },
          (c, t, i) => {
            c2 = c;
            t2 = t;
            progress();
          }
        );
      },
      (c, t, i) => {
        c1 = c;
        t1 = t;
        progress();
      }
    );
  }
  private static initSounds(complete?: Function, onProgress?: (finish: number, total: number, item) => void) {
    this.bundle.loadDir("Sounds/", AudioClip, onProgress, (err, assets: AudioClip[]) => {
      if (err) {
        console.log("initSounds err", err);
        if (complete) complete();
        return;
      }
      assets.forEach((a) => {
        this.sounds.set(a.name, a);
      });
      if (complete) complete();
    });
  }
  private static initSprites(complete?: Function, onProgress?: (finish: number, total: number, item) => void) {
    this.bundle.loadDir("Textures/", SpriteFrame, onProgress, (err, assets: SpriteFrame[]) => {
      if (err) {
        console.log("initSprites err", err);
        if (complete) complete();
        return;
      }
      assets.forEach((a) => {
        this.sprites.set(a.name, a);
      });

      if (complete) complete();
    });
  }

  public static initPopups(complete?: Function, onProgress?: (finish: number, total: number, item) => void) {
    this.bundle.loadDir("Prefabs/Popups/", Prefab, onProgress, (err, assets: Prefab[]) => {
      if (err) {
        console.log("initPopups err", err);
        if (complete) complete();
        return;
      }
      assets.forEach((a) => {
        this.popups.set(a.name, a);
      });
      if (complete) complete();
    });
  }

  private static get bundle(): AssetManager.Bundle {
    return resources;
  }

  public static getPopupPrefab(name: string) {
    if (this.popups.has(name)) {
      return this.popups.get(name);
    } else {
      log("getPopupPrefab FAIL", name);
      return null;
    }
  }

  public static loadScene(sceneName: string) {
    this.bundle.loadScene(sceneName, function (err, scene) {
      if (err) {
        log(err);
      }
      director.runScene(scene);
    });
  }
  public static loadPrefab(path: string, onComplete: (prefab) => void = null) {
    this.bundle.load(path, function (err, prefab) {
      if (err) {
        log(err);
        return;
      }
      if (onComplete) onComplete(prefab);
    });
  }
  public static preloadResourceLocal(paths: string[], onProgress: (finish: number, total: number, item: AssetManager.RequestItem) => void = null, onComplete: () => void = null) {
    if (paths.length == 0) {
      if (onComplete) onComplete();
      return;
    }
    this.bundle.preload(paths, onProgress, function (err, items) {
      if (err) {
        log(err);
        if (onComplete) onComplete();
        return;
      }
      if (onComplete) onComplete();
    });
  }

  public static loadAsset(path: string, type: typeof Asset, onComplete: (image) => void = null) {
    this.bundle.load(path, type, function (err, sprite) {
      if (err) {
        return;
      }
      if (onComplete) onComplete(sprite);
    });
  }

  public static loadSprite(spr: Sprite, number: number) {
    let path = "icon-" + number;
    if (this.sprites.has(path)) {
      spr.spriteFrame = this.sprites.get(path);
    } else {
      this.bundle.load(path + "/spriteFrame", SpriteFrame, function (err, sprF) {
        if (err) {
          log("loadSprite FAIL", path);
          return;
        }
        spr.spriteFrame = sprF;
      });
    }
  }

  public static loadAudio(path: string, onComplete: (audioClip) => void = null) {
    if (this.sounds.has(path)) {
      if (onComplete) onComplete(this.sounds.get(path));
    } else {
      this.bundle.load(path, AudioClip, function (err, audioClip) {
        if (err) {
          log("loadAudio FAIL", path);
          return;
        }
        if (onComplete) onComplete(audioClip);
      });
    }
  }
}
