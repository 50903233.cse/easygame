import {_decorator } from "cc";
import LocalStorageManager from "./LocalStorageManager";
import { GameStateData, UserData } from "../ModelDatas/Data";

const { ccclass, property } = _decorator;
@ccclass("UserManager")
export class UserManager {
  public static get userData() {
    return LocalStorageManager.internalGetUserData("UserData", new UserData(0));
  }

  public static set userData(value: UserData) {
    LocalStorageManager.internalSaveUserData("UserData", value);
  }

  public static saveGameState(userData: UserData, value: GameStateData) {
    let gameState = userData.gameStateData.find((x) => {
      x.level == value.level;
    });
    if(gameState != null){
      gameState.cells = value.cells;
      gameState.level = value.level;
      gameState.matches = value.matches;
      gameState.turns = value.turns;
    }else{
      userData.gameStateData.push(value);
    }

    this.userData = userData;
  }
}
