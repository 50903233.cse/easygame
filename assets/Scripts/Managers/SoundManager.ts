import { _decorator, Component, AudioClip, tween, Tween, director, AudioSource, Node } from 'cc';
const { ccclass, property } = _decorator;

import LocalStorageManager from "./LocalStorageManager";
import ResourceManager from "./ResourceManager";
import { AudioManager } from './AudioManager';

@ccclass('SoundManager')
export default class SoundManager extends Component {
      private static _instance: SoundManager = null;
      public static get instance(): SoundManager {
            if (SoundManager._instance == null) {
                  let canvas = director.getScene().getChildByName("Canvas");
                  let i = canvas.getComponentInChildren(SoundManager);
                  i.loadConfigFromFromStorage();
                  SoundManager._instance = i;
            }
            return SoundManager._instance;
      }
      private KEY_EFFECT_VOLUME: string = "_volume_effect";
      private KEY_EFFECT: string = "_is_effect";
      private KEY_MUSIC_VOLUME: string = "_volume_music";
      private KEY_MUSIC: string = "_is_music";
      private isEnableMusic: boolean = true;
      private isEnableSfx: boolean = true;
      private musicVolume: number = 1;
      private sfxVolume: number = 1;
      public bgMusic: number = -1;
      private currentBgMusic = "";
      private soundEffects: Map<string, number> = new Map<string, number>();
      private sounds_effect_map: Map<string, boolean> = new Map<string, boolean>();
      onDestroy() {
            AudioManager.instance.stopAllEffects();
            AudioManager.instance.stopMusic();
            SoundManager._instance = null;
      }
      public playBgMusicLocalName(name: string, loop: boolean = true, onComplete: () => void = null): void {
            let path = name;
            let self = SoundManager.instance;
            self.playBgMusicByPath(path, loop, onComplete);
      }
      private playBgMusic(audioClip: AudioClip, loop: boolean = true, path: string = "", onComplete: () => void = null): void {
            let self = SoundManager.instance;
            self.currentBgMusic = path;
            self.bgMusic = AudioManager.instance.playMusic(audioClip, loop);
            this.setMusicVolume(this.musicVolume, false);
            Tween.stopAllByTarget(this.node);
      }
      private playBgMusicByPath(path: string, loop: boolean = true, onComplete: () => void = null): void {
            let self = SoundManager.instance;
            if (path == self.currentBgMusic) return;
            self.currentBgMusic = path;
            self.stopMusic();
            ResourceManager.loadAudio(path, (audioClip: AudioClip) => {
                  self.playBgMusic(audioClip, loop, path, onComplete);
            });
      }
      private playSoundEffect(audioClip: AudioClip, loop: boolean = false, path: string, onComplete: () => void = null, numberCb: (audio) => void = null): void {
            let self = SoundManager.instance;
            if (self.SfxVolume != 0) {
                  let number = AudioManager.instance.playEffect(audioClip, loop, () => {
                        if (onComplete) onComplete();
                  });
                  if (numberCb) numberCb(number);
                  self.soundEffects.set(path, number);
            }
      }
      private playEffByPath(path: string, loop: boolean = false, onComplete: () => void = null, numberCb: (audio) => void = null): void {
            let self = SoundManager.instance;
            if (self.SfxVolume != 0) {
                  ResourceManager.loadAudio(path, (audioClip) => {
                        self.playSoundEffect(audioClip, loop, path, onComplete, numberCb);
                  });
            }
      }
      public playEffLocalName(name: string, loop: boolean = false, numberCb: (audio) => void = null, delay: number = 0, onComplete: () => void = null): void {
            let path = name;
            let self = SoundManager.instance;
            if (delay != 0) {
                  tween(this.node)
                        .delay(delay)
                        .call(() => {
                              self.playEffByPath(path, loop, onComplete, numberCb);
                        })
                        .start();
            } else {
                  self.playEffByPath(path, loop, onComplete, numberCb);
            }
      }
      public playOneEffLocalName(name: string, loop: boolean = false,timeOut:number = 0.1, delay: number = 0, numberCb: (audio) => void = null, onComplete: () => void = null): void {
            let path = name;
            let self = SoundManager.instance;
            this.scheduleOnce(() => {
                  this.sounds_effect_map.set(name, false);
            }, timeOut);
            if (!this.sounds_effect_map.has(name) || !this.sounds_effect_map.get(name)) {
                  if (delay != 0) {
                        tween(this.node)
                              .delay(delay)
                              .call(() => {
                                    self.playEffByPath(path, loop, onComplete, numberCb);
                              })
                              .start();
                  } else {
                        self.playEffByPath(path, loop, onComplete, numberCb);
                  }
                  this.sounds_effect_map.set(name, true);
            }
      }
      public loadConfigFromFromStorage() {
            let music: AudioSource = new Node().addComponent(AudioSource);
            let sfx: AudioSource = new Node().addComponent(AudioSource);
            music.node.setParent(this.node);
            sfx.node.setParent(this.node);
            AudioManager.instance.init(sfx, music);
            this.isEnableMusic = this.getMusicStatusFromStorage();
            this.isEnableSfx = this.getSfxStatusFromStorage();
            this.musicVolume = this.getMusicVolumeFromStorage();
            this.sfxVolume = this.getSfxVolumeFromStorage();

            this.setMusicVolume(this.musicVolume);
            this.setSfxVolume(this.sfxVolume);
      }
      setMusicStatus(isOn: boolean) {
            this.isEnableMusic = isOn;
            this.saveMusicStatusToStorage(isOn);
            this.setMusicVolume(this.musicVolume);
      }
      setMusicVolume(volume: number, isSave: boolean = true) {
            this.musicVolume = volume;
            AudioManager.instance.setMusicVolume(this.MusicVolume);
            if (!isSave) return;
            this.saveMusicVolumeToStorage(volume);
      }
      get MusicVolume() {
            return this.isEnableMusic ? this.musicVolume : 0;
      }
      setSfxVolume(volume: number, isSave: boolean = true) {
            this.sfxVolume = volume;
            AudioManager.instance.setEffectsVolume(this.SfxVolume);
            if (!isSave) return;
            this.saveSfxVolumeToStorage(volume);
      }
      setSfxStatus(isOn: boolean) {
            this.isEnableSfx = isOn;
            this.saveSfxStatusToStorage(isOn);
            this.setSfxVolume(this.sfxVolume);
      }
      get SfxVolume() {
            return this.isEnableSfx ? this.sfxVolume : 0;
      }
      //SECTION Storage
      getMusicStatusFromStorage(): boolean {
            return LocalStorageManager.internalGetBoolean(this.KEY_MUSIC, true);
      }
      saveMusicStatusToStorage(isOn: boolean) {
            LocalStorageManager.internalSaveBoolean(this.KEY_MUSIC, isOn);
      }
      getMusicVolumeFromStorage(): number {
            return Number.parseFloat(LocalStorageManager.internalGetString(this.KEY_MUSIC_VOLUME, "1"));
      }
      saveMusicVolumeToStorage(volume: number): void {
            LocalStorageManager.internalSaveString(this.KEY_MUSIC_VOLUME, volume.toString());
      }
      getSfxStatusFromStorage(): boolean {
            return LocalStorageManager.internalGetBoolean(this.KEY_EFFECT, true);
      }
      saveSfxStatusToStorage(isOn: boolean) {
            LocalStorageManager.internalSaveBoolean(this.KEY_EFFECT, isOn);
      }
      getSfxVolumeFromStorage(): number {
            return Number.parseFloat(LocalStorageManager.internalGetString(this.KEY_EFFECT_VOLUME, "1"));
      }
      saveSfxVolumeToStorage(volume: number): void {
            LocalStorageManager.internalSaveString(this.KEY_EFFECT_VOLUME, volume.toString());
      }
      //!SECTION
      public stopMusic(): void {
            AudioManager.instance.stopMusic();
      }
      public pauseMusic(): void {
            AudioManager.instance.pauseMusic();
      }
      public resumeMusic(): void {
            AudioManager.instance.resumeMusic();
      }
      public stopAllEffects(): void {
            AudioManager.instance.stopAllEffects();
      }
      public pauseAllEffects(): void {
            AudioManager.instance.pauseAllEffects();
      }
      public resumeAllEffects(): void {
            AudioManager.instance.resumeAllEffects();
      }
      public stopSoundEffectLocal(name: string): void {
            let path = name;
            let self = SoundManager.instance;
            self.stopSoundEffect(path);
      }
      private stopSoundEffect(path: string): void {
            let self = SoundManager.instance;
            if (self.soundEffects.has(path)) {
                  AudioManager.instance.stopEffect(self.soundEffects.get(path));
            }
      }
      public stopSoundEffectNumber(audioId: number): void {
            AudioManager.instance.stopEffect(audioId);
      }
}

