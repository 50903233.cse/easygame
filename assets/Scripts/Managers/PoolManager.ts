import { _decorator, Node, NodePool } from "cc";
const { ccclass, property } = _decorator;

@ccclass("PoolManager")
export default class PoolManager {
  private static _instance: PoolManager = null;
  public static get instance(): PoolManager {
    if (PoolManager._instance == null) {
      PoolManager._instance = new PoolManager();
    }
    return PoolManager._instance;
  }

  itemPools: Map<string, NodePool> = new Map<string, NodePool>();

  getPool(key: string): Node {
    if (this.itemPools.has(key)) {
      let pool = this.itemPools.get(key);
      if (pool.size() > 0) {
        let node = pool.get();
        this.itemPools.set(key, pool);
        return node;
      }
    }
    return null;
  }
  putPool(key: string, node: Node) {
    let pool = new NodePool();
    if (this.itemPools.has(key)) {
      pool = this.itemPools.get(key);
    }
    pool.put(node);
    this.itemPools.set(key, pool);
  }
}
