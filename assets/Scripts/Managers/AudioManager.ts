import { AudioClip, AudioSource, clamp01, _decorator, Node } from "cc";

const { ccclass, property } = _decorator;
@ccclass("AudioManager")
export class AudioManager {
  private static _instance: AudioManager;
  private static _audioSource?: AudioSource;
  private static _audioSourceMusic?: AudioSource;
  static get instance() {
    if (this._instance) {
      return this._instance;
    }

    this._instance = new AudioManager();
    return this._instance;
  }

  private _audioMap: Map<number, AudioSource> = new Map<number, AudioSource>();

  /**Manager initialization*/
  init(audioSource: AudioSource, audioSourceMusic: AudioSource) {
    AudioManager._audioSource = audioSource;
    AudioManager._audioSourceMusic = audioSourceMusic;
  }

  /**
   * Play music
   * @param {Boolean} loop Whether to loop
   */
  playMusic(audioClip: AudioClip, loop: boolean): number {
    let audioSource = AudioManager._audioSourceMusic;
    if (audioSource == null) return;
    if (!audioSource.playing) {
      audioSource.clip = audioClip;
      audioSource.loop = loop;
      audioSource.play();
    } else {
      audioSource.stop();
      audioSource.clip = audioClip;
      audioSource.loop = loop;
      audioSource.play();
    }
    return 1;
  }

  private audioNumber: number = 0;
  /**
   * Play a sound effect
   * @param {String} name The name of the sound effect
   * @param {Number} volumeScale Playback volume multiplier
   */
  playEffect(audioClip: AudioClip, loop: boolean, callBack: Function): number {
    let audioSource = AudioManager._audioSource;
    if (audioSource == null) return -1;
    if (audioClip == null) return -1;
    let num = -1;
    let audioS: AudioSource = null;
    this._audioMap.forEach((a, key) => {
      if (a.clip && a.playing == false) {
        num = key;
        audioS = a;
      }
    });

    if (audioS == null) {
      num = this._audioMap.size + 1;
      let node = new Node();
      node.name = "audioSource_" + audioClip.name;
      audioS = node.addComponent(AudioSource);
      audioS.node.setParent(audioSource.node.parent);
      this._audioMap.set(num, audioS);
    }
    audioS.loop = loop;
    audioS.clip = audioClip;
    audioS.play();
    audioS.scheduleOnce(callBack, audioClip.getDuration());
    return num;
  }

  // Set the music volume
  setMusicVolume(flag: number) {
    let audioSource = AudioManager._audioSourceMusic;
    if (audioSource == null) return;
    flag = clamp01(flag);
    audioSource.volume = flag;
  }

  // Set the effect volume
  setEffectsVolume(flag: number) {
    flag = clamp01(flag);
    this._audioMap.forEach((audioS) => {
      audioS.volume = flag;
    });
  }

  public stopMusic(): void {
    let audioSource = AudioManager._audioSourceMusic;
    if (audioSource == null) return;
    audioSource.stop();
  }
  public pauseMusic(): void {
    let audioSource = AudioManager._audioSourceMusic;
    if (audioSource == null) return;
    audioSource.pause();
  }
  public resumeMusic(): void {
    let audioSource = AudioManager._audioSourceMusic;
    if (audioSource == null) return;
    audioSource.play();
  }
  public stopAllEffects(): void {
    this._audioMap.forEach((audioS) => {
      audioS.stop();
    });
  }
  public pauseAllEffects(): void {
    this._audioMap.forEach((audioS) => {
      audioS.pause();
    });
  }
  public resumeAllEffects(): void {}
  public stopEffect(audioId: number): void {
    let audioS = this._audioMap.get(audioId);
    if (audioS) {
      audioS.stop();
    }
  }
}
