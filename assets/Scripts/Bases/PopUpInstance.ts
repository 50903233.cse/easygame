import { _decorator, Component, Node, Widget, Animation, Tween, tween, UIOpacity } from "cc";
const { ccclass, property } = _decorator;

import { SoundName } from "../Defines/SoundName";
import SoundManager from "../Managers/SoundManager";

@ccclass("PopUpInstance")
export default class PopUpInstance extends Component {
  @property(Node)
  panel: Node | null = null;
  private widget: Widget;
  _animation: Animation;
  _open = false;
  _data;
  _onYes: () => void;
  _onNo: () => void;
  _close: () => void;
  onDestroy() {
    this.unscheduleAllCallbacks();
  }
  onLoad() {
    this._animation = this.getComponent(Animation);
    this.widget = this.getComponent(Widget);
    this.node.active = false;
  }
  private setWidget() {
    if (!this.widget) return;
    this.widget.isAlignLeft = true;
    this.widget.isAbsoluteRight = true;
    this.widget.isAbsoluteTop = true;
    this.widget.isAlignBottom = true;
    this.widget.left = 0;
    this.widget.top = 0;
    this.widget.bottom = 0;
    this.widget.right = 0;
    this.widget.target = this.node.parent;
  }
  showPopup() {
    this.setWidget();
    if (this.panel != null) {
      this.node.active = true;
      let uiO = this.panel.getComponent(UIOpacity);
      uiO.opacity = 0;
      Tween.stopAllByTarget(uiO);
      tween(uiO)
        .to(0.2, { opacity: 255 })
        .call(() => {
          this.showDone();
        })
        .start();
    } else {
      this.node.active = true;
      if (this._animation != null) {
        this._animation.stop();
        this._animation.play("Show");
      } else {
        this.showDone();
      }
    }
  }
  hidePopup() {
    if (this.panel != null) {
      let uiO = this.panel.getComponent(UIOpacity);
      Tween.stopAllByTarget(uiO);
      tween(uiO)
        .to(0.2, { opacity: 0 })
        .call(() => {
          this.node.active = false;
          this.closeDone();
        })
        .start();
    } else {
      if (this._animation != null) {
        this._animation.stop();
        this._animation.play("Hide");
      } else {
        this.closeDone();
      }
    }
  }
  public open(data, onYes: () => void = null, onNo: () => void = null) {
    this.beforeShow();
    this._open = true;
    this.showPopup();
    this._data = data;
    this._onYes = onYes;
    this._onNo = onNo;
    this.onShow(data);
  }
  public closeInstance() {
    if (!this._open) return;
    this._open = false;
    if (this._close) this._close();
    this.beforeClose();
    this.hidePopup();
  }
  protected close() {
    this._open = false;
    if (this._close) this._close();
    SoundManager.instance.playOneEffLocalName(SoundName.SFX_CLICK, false, 0.5);
    this.beforeClose();
    this.hidePopup();
  }
  public onYes() {
    if (this._onYes) {
      this._onYes();
    }
    this.close();
  }
  public onNo() {
    if (this._onNo) {
      this._onNo();
    }
    this.close();
  }
  public showDone() {
    this.afterShow();
  }
  public closeDone() {
    if (this._open == false) {
      this.node.active = false;
      this.afterClose();
    }
  }
  //#endregion
  protected onShow(data) {}
  protected afterShow() {}
  protected beforeShow() {}
  protected beforeClose() {}
  protected afterClose() {}
}
